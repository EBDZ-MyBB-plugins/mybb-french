<?php
	// MyBB Maintenance Mode plugin U.S. English language file.
	// (C) 2015 CubicleSoft.  All Rights Reserved.

	$l["MaintenanceMode_disabled"] = "The board has been disabled for maintenance.  {1}";
?>