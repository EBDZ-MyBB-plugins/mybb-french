<?php
/**
 * Thank You / Like System English Language Pack
 * 
 */
$l['tyl_thankyou'] = "Merci";
$l['tyl_thankyou_sm'] = "merci";
$l['tyl_thanked'] = "remercié";
$l['tyl_thanked_sm'] = "remerciés";
$l['tyl_thanks'] = "remerciements";
$l['tyl_thanks_sm'] = "remerciements";
$l['tyl_thanksyoufor_sm'] = "vous remercient pour";
$l['tyl_thankyoufor_sm'] = "vous remercie pour";
$l['tyl_like'] = "Like";
$l['tyl_like_sm'] = "like";
$l['tyl_liked'] = "Liked";
$l['tyl_liked_sm'] = "liked";
$l['tyl_likes'] = "Likes";
$l['tyl_likes_sm'] = "likes";
$l['tyl_likesyoufor_sm'] = "likes";
$l['tyl_likeyoufor_sm'] = "like";

$l['tyl_tyls_rcvd'] = "{1} reçus";
$l['tyl_tyls_given'] = "{1} donnés";
$l['tyl_tyls_rcvd_bit'] = "{1} dans {2} messages";

$l['tyl_users'] = "utilisateurs";
$l['tyl_user'] = "utilisateur";
$l['tyl_says'] = "disent";
$l['tyl_say'] = "dit";

$l['tyl_title_ty'] = "<strong>Les {1} utilisateurs suivants disent merci à {4} pour ce message:</strong>";
$l['tyl_title_l'] = "<strong>Les {1} {2} {3} suivants le message de {4}:</strong>";
$l['tyl_title_collapsed_ty'] = "<strong>{1} utilisateurs disent merci à {4} pour ce message</strong>";
$l['tyl_title_collapsed_l'] = "<strong>message de {1} {2} {3} {4}</strong>";

$l['add_ty'] = "Ajouter un Merci";
$l['add_ty_button_title'] = "Ajouter un Merci à ce message";
$l['del_ty'] = "Supprimer le Merci";
$l['del_ty_button_title'] = "Supprimer votre Merci de ce message";

$l['add_l'] = "Like";
$l['add_l_button_title'] = "Like ce message";
$l['del_l'] = "Unlike";
$l['del_l_button_title'] = "Unlike ce message";

$l['tyl_total_tyls_given'] = "Total {1} donnés:";
$l['tyl_total_tyls_rcvd'] = "Total {1} reçus:";
$l['tyl_tylpd_percent_total'] = "{1} par jour | {2} pour cent du total {3}";
$l['tyl_find_tyled_threads'] = "Trouver tous les sujets avec des remerciements donnés";
$l['tyl_find_tyled_posts'] = "Trouver tous les messages avec des remerciements donnés";
$l['tyl_find_tyled_threads_for'] = "Trouver tous les sujets avec des remerciements reçus";
$l['tyl_find_tyled_posts_for'] = "Trouver tous les messages avec des remerciements reçus";

$l['tyl_profile_box_thead'] = "Le message le plus {2} de {1}";
$l['tyl_profile_box_subject'] = "Titre du message";
$l['tyl_profile_box_number'] = "Nombre de {1}";
$l['tyl_profile_box_datetime'] = "Date et heure du message";
$l['tyl_profile_box_message'] = "Contenu du message";
$l['tyl_profile_box_thread'] = "Titre du sujet";
$l['tyl_profile_box_forum'] = "Nom du forum";
$l['tyl_profile_box_continue_reading'] = "Continuer à lire";

$l['tyl_profile_box_content_none'] = "{1} n&raquo;a pas {2} de message pour l&raquo;instant.";

$l['tyl_last_week'    ] = "La semaine dernière";
$l['tyl_last_month'  ] = "Le mois dernier";
$l['tyl_last_3months' ] = "Les 3 derniers mois";
$l['tyl_last_6months' ] = "Les 6 derniers mois";
$l['tyl_last_12months'] = "Les 12 derniers mois";
$l['tyl_all_time'    ] = "Depuis l&raquo;origine";
$l['tyl_profile_stats_head'] = "{2} reçus et donnés à {1}";
$l['tyl_most_tyled_by'] = "Le plus {1} par";
$l['tyl_most_tyled'] = "Les plus {1}";
$l['tyl_no_most_tyledby'] = "{1} n&raquo;a pas encore {2}.";
$l['tyl_no_most_tyled'] = "{1} n&raquo;a pas été {2}.";
$l['tyl_deleted_member_name'] = "[Membre supprimé]";
$l['tyl_pc'] = "{1}%";

$l['tyl_wol_searching'] = "<a href=\"{1}\">Cherche {2}</a>";

$l['tyl_this'] = 'Ce';

$l['tyl_error'] = "Message d'erreur du Thank You/Like System";
$l['tyl_error_invalid_action'] = "Vous essayez d'exécuter une action invalide.";
$l['tyl_error_disabled'] = "La fonctionnalité {1} a été désactivée.";
$l['tyl_error_not_allowed'] = "Non autorisé pour ce message.";
$l['tyl_error_first_post_only'] = "Vous ne pouvez Ajouter/Supprimer {1} que dans le premier message du sujet dans ce forum.";
$l['tyl_error_excluded'] = "Non autorisé pour ce message parce que ce forum a été exclu.";
$l['tyl_error_reached_max_limit'] = "Maximum atteint {1} par jour!";
$l['tyl_error_reached_max_timeleft'] = "Prochain {1} possible dans ";
$l['tyl_error_own_post'] = "Vous ne pouvez {1} Votre propre message.";
$l['tyl_error_hidden_from_group'] = "Les membres de votre groupe d'utilisateurs ne sont pas autorisés tà utiliser le système de {1}.";
$l['tyl_error_already_tyled'] = "Vous avez déjà {1} ce message.";
$l['tyl_error_unknown'] = "Une erreur inconnue à empêché l&raquo;ajout de votre {1}.";
$l['tyl_error_removal_disabled'] = "Vous ne pouvez pas supprimer votre {1} parce que les suppressions ont été désactivées.";
$l['tyl_error_own_delete'] = "Ce {1} ne peut être supprimé parce que vous n&raquo;êtes pas l&raquo;utilisateur qui a posté {1}. Vous ne pouvez supprimer les {1} que vous avez ajouté.";
$l['tyl_error_not_found'] = "Le {1} ne peut être supprimé parcequ&raquo;il n&raquo;a pas été trouvé. Il est possible qu&raquo;il soit déjà supprimé.";
$l['tyl_error_threadclosed'] = "Vous ne pouvez pas {1} ce message parce qu&raquo;il a été verouillé.";
$l['tyl_error_flood_interval_exceeded'] = "Vous ajoutez/supprimez {1} plus vite que permis. Vous devez attendre encore {2} secondes avant d'ajouter/supprimer un autre {3}.";

$l['tyl_redirect_tyled'] = "Votre {1} a été ajouté à ce message";
$l['tyl_redirect_deleted'] = "Votre {1} a été supprimé de ce message";
$l['tyl_redirect_back'] = "<br />Vous allez être redirigé vers le message.";

$l['tyl_alert_one_tyl'] = '{1} {2} votre message<br /><b>"{3}"</b>.';
$l['tyl_alert_two_tyls'] = '{1} et 1 autre ({2} nouveau) {3} votre message<br /><b>"{4}"</b>.';
$l['tyl_alert_three_or_more_tyls'] = '{1} et {2} autres ({3} nouveaux) {4} votre message<br /><b>"{5}"</b>.';
$l['myalerts_setting_tyl'] = 'Recevoir une alerte quand quelqu&raquo;un ajoute un {1} à votre message?';

$l['tyl_send'] = 'Ajouté {1} à ce message';
$l['tyl_remove'] = 'Supprimé {1} de ce message';

$l['tyl_num_left'] = "Vous avez {1} {2} restant à utiliser pour les prochaines 24 Heures.";
$l['tyl_num_left_for'] = "Vous avez {1} {2} restant à utiliser pour un autre {3}.";
$l['tyl_num_left_unlimited'] = "Vous avez {1} illimités restant.";

$l['tyl_firstpost_tyl_count_forumdisplay_thread'] = "Total {1} reçu dans le premier message";
$l['tyl_firstpost_tyl_count_search_page'] = "Total {1} reçu dans le premier message";
