<?php
/**
 * MyBB 1.8 French Language Pack
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 */

// Help Document 1
$l['d1_name'] = "Enregistrement de l’utilisateur";
$l['d1_desc'] = "Avantages et privilèges de l’enregistrement";
$l['d1_document'] = "Quelques parties de ce forum peuvent nécessiter d’être connecté, donc inscrit. L’enregistrement est gratuit et prend seulement quelques minutes.
<br /><br />Vous êtes encouragé à vous enregistrer ; une fois enregistré, vous pourrez poster des messages, définir vos préférences et mettre à jour votre profil.
<br /><br />Les éléments qui requièrent un enregistrement sont les abonnements, la gestion des favoris, le changement de style et de thème, l’accès au bloc-notes personnel et le contact par email des membres du forum.";

// Help Document 2
$l['d2_name'] = "Mise à jour du profil";
$l['d2_desc'] = "Modification de vos données enregistrées.";
$l['d2_document'] = "Parfois, vous pourrez décider de mettre à jour certaines informations, comme par exemple les informations de messagerie instantanée, votre mot de passe ou encore le changement d’adresse email. Vous pouvez changer ces informations depuis le panneau de configuration utilisateur. Pour accéder à ce panneau, cliquez simplement sur le lien en haut de la page intitulé \"Configuration\". Là, choisissez de modifier votre profil et modifiez ou mettez à jour toutes les données que vous désirez, validez ensuite vos changements avec le bouton adéquat en bas de la page pour que les modifications prennent effet.";

// Help Document 3
$l['d3_name'] = "Usage des cookies avec MyBB";
$l['d3_desc'] = "MyBB utilise les cookies pour stocker vos informations d’enregistrement.";
$l['d3_document'] = "MyBB utilise les cookies pour stocker vos informations de connexion, si vous êtes enregistré et votre dernière visite, si vous ne l’êtes pas.
<br /><br />Les cookies sont des petits documents texte stockés sur votre ordinateur ; les cookies de ce forum ne sont utilisés que par ce forum et ne posent aucun problème de sécurité.
<br /><br />Les cookies de ce forum mémorisent vos sujets/messages lus et le moment où vous les avez lus.
<br /><br />Pour supprimer tous les cookies de ce forum, vous pouvez cliquer <a href=\"misc.php?action=clearcookies&amp;my_post_key={1}\">ici</a>.";

// Help Document 4
$l['d4_name'] = "Connexion et déconnexion";
$l['d4_desc'] = "Comment se connecter et se déconnecter.";
$l['d4_document'] = "Quand vous vous connectez, vous créez un cookie sur votre ordinateur, ce qui vous permet de visiter le forum sans avoir à redonner votre nom d’utilisateur et votre mot de passe à chaque fois. La déconnexion supprime les cookies pour s’assurer que personne ne navigue sur le forum avec votre compte.
<br /><br />Pour vous connecter, cliquez simplement sur le bouton ’Connexion’ en haut. Pour vous déconnecter, cliquez sur le bouton au même endroit. Enfin, si vous supprimez les cookies de votre ordinateur, vous serez déconnecté.";

// Help Document 5
$l['d5_name'] = "Création d’un nouveau sujet";
$l['d5_desc'] = "Création d’un nouveau sujet dans un forum";
$l['d5_document'] = "Quand vous êtes intéressé par un forum, vous pouvez créer un nouveau sujet, cliquez simplement sur le bouton situé en haut et en bas intitulé \"Nouveau sujet\". N’oubliez pas que vous n’aurez peut-être pas les permissions de poster dans tous les forums en fonction des restrictions imposées par l’Administration du forum.";

// Help Document 6
$l['d6_name'] = "Poster une réponse";
$l['d6_desc'] = "Répondre à un message.";
$l['d6_document'] = "Au cours de votre visite, vous pouvez rencontrer un sujet auquel vous aimeriez répondre. Pour cela, cliquez sur le bouton situé en haut et en bas intitulé \"Répondre\". N’oubliez pas que vous n’aurez peut-être pas les permissions de poster dans tous les forums en fonction des restrictions imposées par l’Administration du forum.
<br /><br />En outre, un modérateur d’un forum peut verrouiller un sujet, ce qui rend impossible de poster à la suite. Un utilisateur ne peut pas réouvrir un sujet verrouillé sans l’intervention d’un modérateur ou d’un administrateur.";

// Help Document 7
$l['d7_name'] = "MyCode";
$l['d7_desc'] = "Utiliser le MyCode, pour agrémenter vos messages.";
$l['d7_document'] = "Vous pouvez utiliser MyCode, connu aussi comme BBCode, pour ajouter des effets ou du formatage sans cos messages. MyCodes est une version simplifiée du HTML et est utilisé dans un format similaire aux balises HTML que vous connaissez peut-être déjà.
<br /><br />Le tableau ci-dessous est un guide rapide des MyCodes disponibles :
<br /><br />
<table class=\"tborder\" cellspacing=\"0\" cellpadding=\"5\" border=\"0\" style=\"width:90%\">
<tbody>
<tr>
<td class=\"tcat\" style=\"width:55%\"><span class=\"smalltext\"><strong>Input</strong></span></td>
<td class=\"tcat\" style=\"width:35%\"><span class=\"smalltext\"><strong>Output</strong></span></td>
<td class=\"tcat\" style=\"width:10%\"><span class=\"smalltext\"><strong>Notes</strong></span></td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[b]</span>Ce texte est en gras<span style=\"font-weight: bold; color: #ff0000;\">[/b]</span></td>
<td class=\"trow1\"><span style=\"font-weight: bold;\" class=\"mycode_b\">Ce texte est en gras</span></td>
<td class=\"trow1\"></td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[i]</span>Ce texte est en italique<span style=\"font-weight: bold; color: #ff0000;\">[/i]</span></td>
<td class=\"trow2\"><span style=\"font-style: italic;\" class=\"mycode_i\">Ce texte est en italique</span></td>
<td class=\"trow2\"></td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[u]</span>Ce texte est souligné<span style=\"font-weight: bold; color: #ff0000;\">[/u]</span></td>
<td class=\"trow1\"><span style=\"text-decoration: underline;\" class=\"mycode_u\">Ce texte est souligné</span></td>
<td class=\"trow1\"></td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[s]</span>Ce texte est barré<span style=\"font-weight: bold; color: #ff0000;\">[/s]</span></td>
<td class=\"trow2\"><span style=\"text-decoration: line-through;\" class=\"mycode_s\">Ce texte est barré</span></td>
<td class=\"trow2\"></td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[url]</span>http://www.example.com/<span style=\"font-weight: bold; color: #ff0000;\">[/url]</span></td>
<td class=\"trow1\"><a href=\"http://www.example.com/\" class=\"mycode_url\" rel=\"nofollow\">http://www.example.com/</a></td>
<td class=\"trow1\">Les URL seront automatiquement liées si le protocole approprié est inclus (les protocoles valides sont http, https, ftp, news, irc, ircs et irc6).</td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[url=http://www.example.com/]</span>Exemple.com<span style=\"font-weight: bold; color: #ff0000;\">[/url]</span></td>
<td class=\"trow2\"><a href=\"http://www.example.com/\" class=\"mycode_url\" rel=\"nofollow\">Exemple.com</a></td>
<td class=\"trow2\"></td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[email]</span>example@exemple.com<span style=\"font-weight: bold; color: #ff0000;\">[/email]</span></td>
<td class=\"trow1\"><a href=\"mailto:example@example.com\" class=\"mycode_email\">example@exemple.com</a></td>
<td class=\"trow1\"></td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[email=example@example.com]</span>E-mail Me!<span style=\"font-weight: bold; color: #ff0000;\">[/email]</span></td>
<td class=\"trow2\"><a href=\"mailto:example@example.com\" class=\"mycode_email\">Envoyez moi un email !</a></td>
<td class=\"trow2\">Un objet peut être inclus en ajoutant <strong>?subject=Objet ici</strong> après l'adresse e-mail.
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[quote]</span>Le texte cité sera ici<span style=\"font-weight: bold; color: #ff0000;\">[/quote]</span></td>
<td class=\"trow1\"><blockquote class=\"mycode_quote\"><cite>Citation :</cite>Le texte cité sera ici</blockquote></td>
<td class=\"trow1\"></td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[quote='Admin' pid='1' dateline='946684800']</span>Le texte cité sera ici<span style=\"font-weight: bold; color: #ff0000;\">[/quote]</span></td>
<td class=\"trow2\"><blockquote class=\"mycode_quote\"><cite><span> (01-01-2000, 12:00 AM)</span>Admin a écrit : <a href=\"http://www.example.com/showthread.php?pid=1#pid1\" class=\"quick_jump\" rel=\"nofollow\"></a></cite>Le texte cité sera ici</blockquote></td>
<td class=\"trow2\">Ce format est utilisé pour citer des messages. <strong>pid</strong> renvoie à un article, <strong>dateline</strong> est un <a href=\"https://www.unixtimestamp.com/\">horodatage UNIX</a>.</td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[code]</span>Texte avec mise en forme préservée<span style=\"font-weight: bold; color: #ff0000;\">[/code]</span></td>
<td class=\"trow1\"><div class=\"codeblock\"><div class=\"title\">Code:</div><div class=\"body\" dir=\"ltr\"><code>Texte avec mise en forme préservée </code></div></div></td>
<td class=\"trow1\"></td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[php]</span>&lt;?php echo \"Hello world!\";?&gt;<span style=\"font-weight: bold; color: #ff0000;\">[/php]</span></td>
<td class=\"trow2\"><div class=\"codeblock phpcodeblock\"><div class=\"title\">PHP Code:</div><div class=\"body\"><div dir=\"ltr\"><code><span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #DD0000\">\"Hello&nbsp;world!\"</span><span style=\"color: #007700\">;</span><span style=\"color: #0000BB\">?&gt;</span></code></div></div></div></div></td>
<td class=\"trow2\"></td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[img]</span>https://secure.php.net/images/php.gif<span style=\"font-weight: bold; color: #ff0000;\">[/img]</span></td>
<td class=\"trow1\"><img src=\"https://secure.php.net/images/php.gif\" class=\"mycode_img\"></td>
<td class=\"trow1\"></td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[img=50x50]</span>https://secure.php.net/images/php.gif<span style=\"font-weight: bold; color: #ff0000;\">[/img]</span></td>
<td class=\"trow2\"><img src=\"https://secure.php.net/images/php.gif\" width=\"50\" height=\"50\" class=\"mycode_img\"></td>
<td class=\"trow2\">Format is width x height</td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[color=red]</span>Ce texte est en rouge<span style=\"font-weight: bold; color: #ff0000;\">[/color]</span></td>
<td class=\"trow1\"><span style=\"color: red;\" class=\"mycode_color\">Ce texte est en rouge</span></td>
<td class=\"trow1\">Peut utiliser soit <a href=\"https://www.w3schools.com/cssref/css_colors.asp\">un nom de couleur CSS</a> ou un code HEXA.</td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[size=large]</span>Ce tesxte est grand<span style=\"font-weight: bold; color: #ff0000;\">[/size]</span></td>
<td class=\"trow2\"><span style=\"font-size: large\" class=\"mycode_size\">Ce texte est grand</span></td>
<td class=\"trow2\">Valeurs acceptées : xx-small, x-small, small, medium, large, x-large, xx-large</td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[size=30]</span>Ce texte mesure 30 pixels<span style=\"font-weight: bold; color: #ff0000;\">[/size]</span></td>
<td class=\"trow1\"><span style=\"font-size: 30px\" class=\"mycode_size\">Ce texte mesure 30 pixels</span></td>
<td class=\"trow1\">Accepte un nombre de 1 à 50</td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[font=Impact]</span>Cette police est Impact<span style=\"font-weight: bold; color: #ff0000;\">[/font]</span></td>
<td class=\"trow2\"><span style=\"font-family: Impact;\" class=\"mycode_font\">Cette police est Impact</span></td>
<td class=\"trow2\">La police doit être installée sur votre ordinateur</td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[align=center]</span>Ceci est centré<span style=\"font-weight: bold; color: #ff0000;\">[/align]</span></td>
<td class=\"trow1\"><div style=\"text-align: center;\" class=\"mycode_align\">Ceci est centré</div></td>
<td class=\"trow1\">Valeurs acceptées : left, center, right, justify</td>
</tr>
<tr>
<td class=\"trow2\"><span style=\"font-weight: bold; color: #ff0000;\">[list]</span><br />[*]Élément de liste #1<br />[*]Élément de liste #2<br />[*]Élément de liste #3<br /><span style=\"font-weight: bold; color: #ff0000;\">[/list]</span></td>
<td class=\"trow2\"><ul class=\"mycode_list\"><li>Élément de liste #1</li><li>Élément de liste #2</li><li>Élément de liste #3</li></ul></td>
<td class=\"trow2\"></td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[list=1]</span><br />[*]Élément de liste #1<br />[*]Élément de liste #2<br />[*]Élément de liste #3<br /><span style=\"font-weight: bold; color: #ff0000;\">[/list]</span></td>
<td class=\"trow1\"><ol class=\"mycode_list\" type=\"1\"><li>Élément de liste #1</li><li>Élément de liste #2</li><li>Élément de liste #3</li></ol></td>
<td class=\"trow1\"><strong>1</strong> peut être utilisé pour une liste numérotée, <strong>a</strong> pour une liste alphabétique, <strong>i</strong> pour une liste de chiffres romains.</td>
</tr>
<tr>
<td class=\"trow2\">Une ligne qui<span style=\"font-weight: bold; color: #ff0000;\">[hr]</span>sépare</td>
<td class=\"trow2\">Une ligne qui<hr class=\"mycode_hr\">sépare</td>
<td class=\"trow2\"></td>
</tr>
<tr>
<td class=\"trow1\"><span style=\"font-weight: bold; color: #ff0000;\">[video=youtube]</span>https://www.youtube.com/watch?v=dQw4w9WgXcQ<span style=\"font-weight: bold; color: #ff0000;\">[/video]</span></td>
<td class=\"trow1\"><iframe src=\"//www.youtube.com/embed/dQw4w9WgXcQ\" allowfullscreen=\"\" width=\"460\" height=\"255\" frameborder=\"0\"></iframe></td>
<td class=\"trow1\">Actuellement accepte Dailymotion, Facebook, LiveLeak, Metacafe, Mixer, MySpace TV, Twitch, Vimeo, Yahoo Videos et YouTube.</td>
</tr>
</tbody></table>
<br /><br />De plus, les administrateurs peuvent avoir créé plus de MyCodes à utiliser. Veuillez contacter un administrateur pour savoir s’il y en a et comment les utiliser.";